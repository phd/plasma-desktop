# Translation of plasma_applet_org.kde.plasma.kicker.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2014, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.kicker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-06-17 02:10+0200\n"
"PO-Revision-Date: 2017-09-25 19:53+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: package/contents/config/config.qml:26
#, kde-format
msgid "General"
msgstr "Opšte"

#: package/contents/ui/code/tools.js:55
#, kde-format
msgid "Remove from Favorites"
msgstr "Ukloni iz omiljenih"

#: package/contents/ui/code/tools.js:59
#, kde-format
msgid "Add to Favorites"
msgstr "Dodaj u omiljene"

#: package/contents/ui/code/tools.js:83
#, kde-format
msgid "On All Activities"
msgstr "Na svim aktivnostima"

#: package/contents/ui/code/tools.js:133
#, fuzzy, kde-format
#| msgid "On The Current Activity"
msgid "On the Current Activity"
msgstr "Na tekućoj aktivnosti"

#: package/contents/ui/code/tools.js:147
#, fuzzy, kde-format
#| msgid "Show In Favorites"
msgid "Show in Favorites"
msgstr "Prikaži u omiljenima"

#: package/contents/ui/ConfigGeneral.qml:59
#, kde-format
msgid "Icon:"
msgstr "Ikonica:"

#: package/contents/ui/ConfigGeneral.qml:139
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose..."
msgstr "Izaberi..."

#: package/contents/ui/ConfigGeneral.qml:144
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Očisti ikonicu"

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgid "Show applications as:"
msgstr "Prikazuj programe kao:"

# >> @item:inlistbox
#: package/contents/ui/ConfigGeneral.qml:164
#, kde-format
msgid "Name only"
msgstr "samo ime"

# >> @item:inlistbox
#: package/contents/ui/ConfigGeneral.qml:164
#, kde-format
msgid "Description only"
msgstr "samo opis"

# >> @item:inlistbox
#: package/contents/ui/ConfigGeneral.qml:164
#, kde-format
msgid "Name (Description)"
msgstr "ime (opis)"

# >> @item:inlistbox
#: package/contents/ui/ConfigGeneral.qml:164
#, kde-format
msgid "Description (Name)"
msgstr "opis (ime)"

# >> @title:group
#: package/contents/ui/ConfigGeneral.qml:174
#, fuzzy, kde-format
#| msgid "Behavior"
msgid "Behavior:"
msgstr "Ponašanje"

#: package/contents/ui/ConfigGeneral.qml:176
#, fuzzy, kde-format
#| msgid "Sort alphabetically"
msgid "Sort applications alphabetically"
msgstr "Poređaj abecedno"

# >> @option:check
#: package/contents/ui/ConfigGeneral.qml:184
#, fuzzy, kde-format
#| msgid "Flatten menu to a single level"
msgid "Flatten sub-menus to a single level"
msgstr "Meni spljošten na jedan nivo"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:202
#, fuzzy, kde-format
#| msgid "Categories"
msgid "Show categories:"
msgstr "Kategorije"

#: package/contents/ui/ConfigGeneral.qml:205
#, fuzzy, kde-format
#| msgid "Recent Applications"
msgid "Recent applications"
msgstr ""
"Nedavni programi|/|$[svojstva gen 'nedavnih programa' aku 'nedavne programe']"

#: package/contents/ui/ConfigGeneral.qml:206
#, fuzzy, kde-format
#| msgid "Often Used Applications"
msgid "Often used applications"
msgstr ""
"Često korišćeni programi|/|$[svojstva gen 'često korišćenih programa' aku "
"'često korišćene programe']"

#: package/contents/ui/ConfigGeneral.qml:213
#, fuzzy, kde-format
#| msgid "Recent Documents"
msgid "Recent documents"
msgstr ""
"Nedavni dokumenti|/|$[svojstva gen 'nedavnih dokumenata' aku 'nedavne "
"dokumente']"

#: package/contents/ui/ConfigGeneral.qml:214
#, fuzzy, kde-format
#| msgid "Often Used Documents"
msgid "Often used documents"
msgstr ""
"Često korišćeni dokumenti|/|$[svojstva gen 'često korišćenih dokumenata' aku "
"'često korišćene dokumente']"

#: package/contents/ui/ConfigGeneral.qml:221
#, fuzzy, kde-format
#| msgid "Recent Contacts"
msgid "Recent contacts"
msgstr ""
"Nedavni kontakti|/|$[svojstva gen 'nedavnih kontakata' aku 'nedavne "
"kontakte']"

#: package/contents/ui/ConfigGeneral.qml:222
#, fuzzy, kde-format
#| msgid "Show often used contacts"
msgid "Often used contacts"
msgstr "Prikaži često korišćene kontakte"

#: package/contents/ui/ConfigGeneral.qml:228
#, kde-format
msgid "Sort items in categories by:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:229
#, fuzzy, kde-format
#| msgid "Recently used"
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr "Nedavno korišćeno"

#: package/contents/ui/ConfigGeneral.qml:229
#, fuzzy, kde-format
#| msgid "Often used"
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr "Često korišćeno"

# >> @title:group
#: package/contents/ui/ConfigGeneral.qml:239
#, fuzzy, kde-format
#| msgid "Search"
msgid "Search:"
msgstr "Pretraga"

# >> @option:check
#: package/contents/ui/ConfigGeneral.qml:241
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "Pretražuj i obeleživače, fajlove i e‑poštu"

# >> @option:check
#: package/contents/ui/ConfigGeneral.qml:249
#, kde-format
msgid "Align search results to bottom"
msgstr "Ravnaj rezultate pretrage uz dno"

# >> @info:progress
#: package/contents/ui/DashboardRepresentation.qml:305
#, kde-format
msgid "Searching for '%1'"
msgstr "Tražim „%1“..."

#: package/contents/ui/DashboardRepresentation.qml:305
#, fuzzy, kde-format
#| msgid "Type to search."
msgid "Type to search..."
msgstr "Unesite nešto za traženje."

#: package/contents/ui/DashboardRepresentation.qml:405
#, kde-format
msgid "Favorites"
msgstr "Omiljeno"

#: package/contents/ui/DashboardRepresentation.qml:622
#: package/contents/ui/DashboardTabBar.qml:55
#, kde-format
msgid "Widgets"
msgstr "Vidžeti"

#: package/contents/ui/DashboardTabBar.qml:44
#, kde-format
msgid "Apps & Docs"
msgstr "Programi i dokumentacija"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Edit Applications..."
msgstr "Uredi programe..."

#: package/contents/ui/MenuRepresentation.qml:318
#, kde-format
msgid "Search..."
msgstr "Traži..."
