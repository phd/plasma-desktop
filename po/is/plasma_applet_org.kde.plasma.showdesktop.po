# translation of plasma_applet_showdesktop.po to
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_showdesktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2011-03-24 09:47+0000\n"
"Last-Translator: \n"
"Language-Team:  <en@li.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"

#: package/contents/ui/MinimizeAllController.qml:17
#, kde-format
msgctxt "@action:button"
msgid "Restore All Minimized Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:18
#, kde-format
msgctxt "@action:button"
msgid "Minimize All Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:20
#, kde-format
msgctxt "@info:tooltip"
msgid "Restores the previously minimized windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:21
#, kde-format
msgctxt "@info:tooltip"
msgid "Shows the Desktop by minimizing all windows"
msgstr ""

#: package/contents/ui/PeekController.qml:13
#, kde-format
msgctxt "@action:button"
msgid "Stop Peeking at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:14
#, kde-format
msgctxt "@action:button"
msgid "Peek at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:16
#, kde-format
msgctxt "@info:tooltip"
msgid "Moves windows back to their original positions"
msgstr ""

#: package/contents/ui/PeekController.qml:17
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Temporarily reveals the Desktop by moving open windows into screen corners"
msgstr ""

#, fuzzy
#~| msgid "Show the Desktop"
#~ msgid "Show Desktop"
#~ msgstr "Sýna skjáborðið"

#, fuzzy
#~| msgid "Show the Desktop"
#~ msgid "Show the Plasma desktop"
#~ msgstr "Sýna skjáborðið"

#~ msgid "Minimize all open windows and show the Desktop"
#~ msgstr "Minnka alla glugga og sýna skjáborðið"
