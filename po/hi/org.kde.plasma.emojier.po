# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2022-02-26 14:06+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: Hindi <kde-l10n-hi@kde.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "इमोजी चयनक"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) २०१९ एलिक्स पॉल गोंज़ालेज़"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "राघवेंद्र कामत"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raghu@raghukamath.com"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "एक वर्तमान चलित अनुप्रयोग को प्रतिस्थापित करें"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:57
#, kde-format
msgid "Search"
msgstr "खोजें"

#: app/ui/CategoryPage.qml:74
#, kde-format
msgid "Clear History"
msgstr "इतिहास साफ करें"

#: app/ui/CategoryPage.qml:153
#, kde-format
msgid "No recent Emojis"
msgstr "कोई हालिया इमोजी नहीं"

#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 की नक़ल क्लिपबोर्ड पर की गइ"

#: app/ui/emojier.qml:47
#, kde-format
msgid "Recent"
msgstr "हालिया"

#: app/ui/emojier.qml:68
#, kde-format
msgid "All"
msgstr "सभी"

#: app/ui/emojier.qml:76
#, kde-format
msgid "Categories"
msgstr "श्रेणियाँ"

#: emojicategory.cpp:11
#, kde-format
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr ""

#: emojicategory.cpp:12
#, kde-format
msgctxt "Emoji Category"
msgid "People and Body"
msgstr ""

#: emojicategory.cpp:13
#, kde-format
msgctxt "Emoji Category"
msgid "Component"
msgstr ""

#: emojicategory.cpp:14
#, kde-format
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr ""

#: emojicategory.cpp:15
#, kde-format
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr ""

#: emojicategory.cpp:16
#, kde-format
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr ""

#: emojicategory.cpp:17
#, kde-format
msgctxt "Emoji Category"
msgid "Activities"
msgstr ""

#: emojicategory.cpp:18
#, kde-format
msgctxt "Emoji Category"
msgid "Objects"
msgstr ""

#: emojicategory.cpp:19
#, kde-format
msgctxt "Emoji Category"
msgid "Symbols"
msgstr ""

#: emojicategory.cpp:20
#, kde-format
msgctxt "Emoji Category"
msgid "Flags"
msgstr ""

#~ msgid "Search…"
#~ msgstr "खोजें…"
