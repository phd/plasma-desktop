# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2022-09-02 07:51+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "Emoji -ების არჩევა"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "გაშვებული ასლის ჩანაცვლება"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:57
#, kde-format
msgid "Search"
msgstr "ძებნა"

#: app/ui/CategoryPage.qml:74
#, kde-format
msgid "Clear History"
msgstr "ისტორიის გაწმენდა"

#: app/ui/CategoryPage.qml:153
#, kde-format
msgid "No recent Emojis"
msgstr "ემოჯიების გარეშე"

#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "დაკოპირდა გაცვლის ბაფერში: %1"

#: app/ui/emojier.qml:47
#, kde-format
msgid "Recent"
msgstr "Ბოლო"

#: app/ui/emojier.qml:68
#, kde-format
msgid "All"
msgstr "ყველა"

#: app/ui/emojier.qml:76
#, kde-format
msgid "Categories"
msgstr "კატეგორიები"

#: emojicategory.cpp:11
#, kde-format
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr ""

#: emojicategory.cpp:12
#, kde-format
msgctxt "Emoji Category"
msgid "People and Body"
msgstr ""

#: emojicategory.cpp:13
#, kde-format
msgctxt "Emoji Category"
msgid "Component"
msgstr ""

#: emojicategory.cpp:14
#, kde-format
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr ""

#: emojicategory.cpp:15
#, kde-format
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr ""

#: emojicategory.cpp:16
#, kde-format
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr ""

#: emojicategory.cpp:17
#, kde-format
msgctxt "Emoji Category"
msgid "Activities"
msgstr ""

#: emojicategory.cpp:18
#, kde-format
msgctxt "Emoji Category"
msgid "Objects"
msgstr ""

#: emojicategory.cpp:19
#, kde-format
msgctxt "Emoji Category"
msgid "Symbols"
msgstr ""

#: emojicategory.cpp:20
#, kde-format
msgctxt "Emoji Category"
msgid "Flags"
msgstr ""
