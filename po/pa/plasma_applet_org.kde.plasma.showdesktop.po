# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Amanpreet Singh Alam <aalam@users.sf.net>, 2008, 2015.
# A S Alam <alam.yellow@gmail.com>, 2018, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_showdesktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2021-10-09 09:22-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/MinimizeAllController.qml:17
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Minimize All Windows"
msgctxt "@action:button"
msgid "Restore All Minimized Windows"
msgstr "ਸਭ ਵਿੰਡੋਆਂ ਨੂੰ ਘੱਟੋ-ਘੱਟ ਕਰੋ"

#: package/contents/ui/MinimizeAllController.qml:18
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Minimize All Windows"
msgctxt "@action:button"
msgid "Minimize All Windows"
msgstr "ਸਭ ਵਿੰਡੋਆਂ ਨੂੰ ਘੱਟੋ-ਘੱਟ ਕਰੋ"

#: package/contents/ui/MinimizeAllController.qml:20
#, kde-format
msgctxt "@info:tooltip"
msgid "Restores the previously minimized windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:21
#, fuzzy, kde-format
#| msgid "Show the desktop by moving windows aside"
msgctxt "@info:tooltip"
msgid "Shows the Desktop by minimizing all windows"
msgstr "ਵਿੰਡੋਆਂ ਨੂੰ ਆਸੇ ਪਾਸੇ ਕਰਕੇ ਡੈਸਕਟਾਪ ਵੇਖਾਓ"

#: package/contents/ui/PeekController.qml:13
#, kde-format
msgctxt "@action:button"
msgid "Stop Peeking at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:14
#, kde-format
msgctxt "@action:button"
msgid "Peek at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:16
#, kde-format
msgctxt "@info:tooltip"
msgid "Moves windows back to their original positions"
msgstr ""

#: package/contents/ui/PeekController.qml:17
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Temporarily reveals the Desktop by moving open windows into screen corners"
msgstr ""

#~ msgid "Show Desktop"
#~ msgstr "ਡੈਸਕਟਾਪ ਦਿਖਾਓ"

#~ msgid "Show the Plasma desktop"
#~ msgstr "ਪਲਾਜ਼ਮਾ ਡੈਸਕਟਾਪ ਵੇਖੋ"

#~ msgid "Minimize all open windows and show the Desktop"
#~ msgstr "ਸਭ ਖੁੱਲ੍ਹੀਆਂ ਵਿੰਡੋ ਘੱਟੋ-ਘੱਟ ਕਰੋ ਅਤੇ ਡੈਸਕਟਾਪ ਵੇਖਾਓ"
