# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2014, 2017, 2018, 2019, 2020, 2021, 2022.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-10 00:47+0000\n"
"PO-Revision-Date: 2022-06-20 10:20+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.2\n"

#: package/contents/ui/main.qml:24
#, kde-format
msgid "Visual behavior:"
msgstr "Visueel gedrag:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: package/contents/ui/main.qml:25 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Informatieve tekstballonnen tonen bij erboven zweven met de muis"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: package/contents/ui/main.qml:37 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Visuele terugkoppeling voor statuswijzigingen tonen"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Animation speed:"
msgstr "Animatiesnelheid:"

#: package/contents/ui/main.qml:78
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Traag"

#: package/contents/ui/main.qml:84
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Snel"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Op bestanden of mappen klikken:"

#: package/contents/ui/main.qml:100
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Ze openen"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Selecteren door te klikken op de selectiemarkering van het item"

#: package/contents/ui/main.qml:123
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Ze selecteren"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Open by double-clicking instead"
msgstr "In plaats daarvan door dubbelklikken openen"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "Klikken in schuifbalktrack:"

#: package/contents/ui/main.qml:152
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "Schuift één pagina omhoog of omlaag"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "Middelste muisklik om te schuiven naar locatie waarop is geklikt"

#: package/contents/ui/main.qml:175
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "Schuift naar de locatie waarop is geklikt"

#: package/contents/ui/main.qml:194
#, kde-format
msgid "Middle Click:"
msgstr "Middelste muisklik:"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Paste selected text"
msgstr "Geselecteerde tekst plakken"

#: package/contents/ui/main.qml:213
#, kde-format
msgid "Touch Mode:"
msgstr "Aanraakmodus:"

#: package/contents/ui/main.qml:215
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "Automatisch inschakelen zoals nodig"

#: package/contents/ui/main.qml:215 package/contents/ui/main.qml:244
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "Nooit ingeschakeld"

#: package/contents/ui/main.qml:227
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"Aanraakmodus zal automatisch geactiveerd worden wanneer het systeem een "
"aanraakscherm detecteert maar geen muis of touchpad. Bijvoorbeeld: wanneer "
"een te transformeren toetsenbord van een laptop wordt afgekoppeld."

#: package/contents/ui/main.qml:232
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "Altijd ingeschakeld"

#: package/contents/ui/main.qml:257
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"In aanraakmodus zullen veel elementen van het gebruikersinterface groter "
"worden om gemakkelijker aanraakinteractie te kunnen doen."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "Enkele klik om bestanden te openen"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "Animatiesnelheid"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr ""
"Linkerklik in het schuifbalkvak verplaatst de schuifbalk met een pagina"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "Automatisch naar aanraakmodus omschakelen"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "Plakken van selectie met middelste muisklik inschakelen"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"Toon iets op het scherm (IoS) om statuswijzigingen aan te geven zoals "
"helderheid of volume"

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "IoS bij wijziging van indeling"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "Een pop-up tonen bij wijziging van indeling"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - t/m 2022"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "General Behavior"
#~ msgstr "Algemeen gedrag"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr ""
#~ "Module van systeeminstellingen voor configureren van algemeen gedrag in "
#~ "werkruimte."

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Never optimize for touch usage"
#~ msgstr "Nooit optimaliseren voor gebruik met aanraken"

#~ msgid "Always optimize for touch usage"
#~ msgstr "Altijd optimaliseren voor gebruik met aanraken"

#~ msgid "Click behavior:"
#~ msgstr "Gedrag van klikken:"

#~ msgid "Double-click to open files and folders"
#~ msgstr "Dubbelklik om bestanden en mappen te openen"

#~ msgid "Select by single-clicking"
#~ msgstr "Selecteren door er eenmaal op te klikken"

#~ msgid "Double-click to open files and folders (single click to select)"
#~ msgstr ""
#~ "Dubbelklik om bestanden en mappen te openen (enkele klik om te selecteren)"

#~ msgid "Plasma Workspace global options"
#~ msgstr "Globale opties van Plasma-werkgebied"

#~ msgid "(c) 2009 Marco Martin"
#~ msgstr "(c) 2009 Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Maintainer"
#~ msgstr "Onderhouder"

#~ msgid "Show Informational Tips"
#~ msgstr "Informatieverstrekkende tips tonen"
